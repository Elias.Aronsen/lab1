package INF102.lab1.triplicate;

import java.util.Collections;
import java.util.List;

public class MyTriplicate<T extends Comparable<T>> implements ITriplicate<T> {

    @Override
    public T findTriplicate(List<T> list) {
        Collections.sort(list);
        int n = list.size();

        //Could be done faster with HashMap

        for (int i = 0; i < (n-2); i++ ){
            if(list.get(i).equals(list.get(i+1)) ){
                if(list.get(i+1).equals(list.get(i+2)) ){
                    return list.get(i);

                } else {
                    i++;
                }
            }
        }   return null;
    }  
}
